# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.10](https://github.com/allcapsdev/cvcsv-cli/compare/v1.1.9...v1.1.10) (2020-03-03)

### Bug Fixes

- **codeclimate:** forgot to update the jest settings ([0aa2afc](https://github.com/allcapsdev/cvcsv-cli/commit/0aa2afcba7e47f70c3e909a292de376adec97bd0))
- **test:** fixed tests based on Code Climate findings ([392f239](https://github.com/allcapsdev/cvcsv-cli/commit/392f239857be60ce3a6f5d9e252cd2aeecf6f644))

### [1.1.9](https://github.com/allcapsdev/cvcsv-cli/compare/v1.1.8...v1.1.9) (2020-02-25)

### [1.1.8](https://github.com/allcapsdev/cvcsv-cli/compare/v1.1.5...v1.1.8) (2020-02-17)

### [1.1.7](https://github.com/allcapsdev/cvcsv-cli/compare/v1.1.5...v1.1.7) (2020-02-11)

### [1.1.6](https://github.com/allcapsdev/cvcsv-cli/compare/v1.1.5...v1.1.6) (2020-02-10)

### [1.1.5](https://github.com/allcapsdev/cvcsv-cli/compare/v1.1.4...v1.1.5) (2020-02-05)

### [1.1.4](https://github.com/allcapsdev/cvcsv-cli/compare/v1.1.3...v1.1.4) (2020-02-04)

### [1.1.3](https://github.com/allcapsdev/cvcsv-cli/compare/v1.1.2...v1.1.3) (2020-01-31)

### [1.1.2](https://github.com/allcapsdev/cvcsv-cli/compare/v1.1.1...v1.1.2) (2020-01-23)

### [1.1.1](https://github.com/allcapsdev/cvcsv-cli/compare/v1.1.0...v1.1.1) (2019-12-27)
